﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WebSpiderLib.Explore
{
    /// <summary>
    /// Définition d'un type de fonction pour un filtre
    /// </summary>
    /// <param name="uri">Adresse a filtrer</param>
    /// <returns></returns>
    public delegate bool FilterDelegate(Uri uri);

    /// <summary>
    /// 
    /// </summary>
    public class WebExplorator
    {
        /// <summary>
        /// HashSet assurant l'unicité des liens parcouru
        /// </summary>
        private readonly HashSet<Uri> _uriSet = new HashSet<Uri>();

        /// <summary>
        /// Buffer stockant les uri en attente de traitement
        /// </summary>
        private readonly Queue<Uri> _uriBuffer = new Queue<Uri>();

        /// <summary>
        /// Pages en attente d'une réponse
        /// </summary>
        private readonly List<Uri> _loadingPages = new List<Uri>(); 

        /// <summary>
        /// Filtre applicable sur les paramètres get de l'uri
        /// </summary>
        private readonly string[] _explorationGetFilter;

        /// <summary>
        /// Uri de départ
        /// </summary>
        private readonly string _startUrl;

        /// <summary>
        /// Nombre de requette maximales
        /// </summary>
        private int MaxRequest = 1;



        public event Action<WebPage> PageLoaded;
        public event Action<Uri> PageError; 
        public event Action<Uri> PageFound; 

        /// <summary>
        /// Constructeur d'un explorateur web
        /// </summary>
        /// <param name="startUrl">Url de départ</param>
        /// <param name="explorationGetFilter">Filtre applicable sur les paramètres get des url allant être parcouru ex : { id, nom } ne prendra que les pages avec un paramètre id ou nom ou les 2</param>
        public WebExplorator(string startUrl, string[] explorationGetFilter)
        {
            _startUrl = startUrl;
            _explorationGetFilter = explorationGetFilter;
        }

        /// <summary>
        /// Démarrage de l'explorateur web (explore la première page, en trouve d'autres respectant le filtre, les explore de la meme manière recursivement)
        /// </summary>
        public void Start()
        {
            Explore(new Uri(_startUrl));
        }

        /// <summary>
        ///     Fonction d'exploration d'une page web (RECURSIF & ASYNC)
        ///         Condition du traitement d'une page : 
        ///          -inexplorée par l'instance courante
        ///          -respect du filtre
        ///          -Absolue
        /// </summary>
        /// <param name="uri"></param>
        private void Explore(Uri uri)
        {
            //Condition du traitement d'une page : 
            //inexplorée par l'instance courante
            //respect du filtre
            //Absolue
            if (!_uriSet.Add(uri) || !ExplorationFilter(uri) || !uri.IsAbsoluteUri)
                return;
            PageFound?.Invoke(uri);
            if (_loadingPages.Count > MaxRequest)
                FillBuffer(uri);
            else
                WebRequest(uri);
        }

        /// <summary>
        /// Filtre Get sur la page web
        /// </summary>
        /// <param name="uri"></param>
        /// <returns></returns>
        private bool ExplorationFilter(Uri uri)
        {
            string href = uri.AbsoluteUri;
            var getParam = uri.ParseQueryString();
            if (!href.Contains('#') && href.Contains(_startUrl))
            {
                if (getParam.Count != 0)
                {
                    foreach (var param in getParam)
                    {
                        foreach (var filter in _explorationGetFilter)
                        {
                            if (param.Key == filter)
                                return true;
                        }
                    }
                    return false;
                }
                return true;
            }
            return false;
        }

        /// <summary>
        ///     Fonction de remplissage du buffer
        /// </summary>
        /// <param name="uri"></param>
        private void FillBuffer(Uri uri)
        {
            _uriBuffer.Enqueue(uri);
        }

        /// <summary>
        ///     Genere une requette web asynchrone 
        /// </summary>
        /// <param name="uri"></param>
        private void WebRequest(Uri uri)
        {
            WebPageLoader.Load(uri, LoadSuccess, LoadError);
            _loadingPages.Add(uri);
        }

        private void LoadSuccess(WebPage webPage)
        {
            PageLoaded?.Invoke(webPage);
            foreach (var uri in webPage.Links)
                Explore(uri);
            _loadingPages.Remove(webPage.Adress);
            if (_uriBuffer.Count != 0)
                WebRequest(_uriBuffer.Dequeue());
        }

        private void LoadError(Uri uri)
        {
            _loadingPages.Remove(uri);
            PageError?.Invoke(uri);
        }
    }
}
